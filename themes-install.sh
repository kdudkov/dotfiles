#!/bin/sh

sudo add-apt-repository ppa:tista/adapta
sudo add-apt-repository ppa:papirus/papirus
sudo add-apt-repository ppa:noobslab/themes
sudo add-apt-repository ppa:tiheum/equinox
sudo add-apt-repository ppa:moka/daily

sudo apt update

sudo apt install adapta-gtk-theme papirus-icon-theme arc-theme faenza-icon-theme moka-icon-theme

