#!/bin/bash

for f in $(cat files_to_save); do
	cp $HOME/$f $f
done;

for k in dunst polybar bspwm sxhkd rofi alacritty ranger sway mako; do
	mkdir -p .config/$k
	cp -r $HOME/.config/$k .config/
done

rm .config/polybar/config

for k in .Xresources.d; do
	mkdir -p $k
	cp $HOME/$k/* $k/
done

for k in colors snippets ftdetect ftplugin syntax; do
	cp -r $HOME/.vim/$k/* .vim/$k/
done

git status
git diff --stat

