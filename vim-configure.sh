#!/bin/bash

echo "srart to configure vim"

for s in colors snippets syntax ftdetect ftplugin bundle undo swap backup; do
	mkdir -p $HOME/.vim/$s
done

cp -u -v -r .vim $HOME/
cp -u -v .vimrc $HOME/

