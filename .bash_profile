os=`uname -s`

if [ "$os" == "Darwin" ]; then
	echo "Darwin"
	[ -f "$HOME/.bash_ext" ] && source "$HOME/.bash_ext"
else
	# include .bashrc if it exists
	if [ -f "$HOME/.bashrc" ]; then
		source "$HOME/.bashrc"
	else
		[ -f "$HOME/.bash_ext" ] && source "$HOME/.bash_ext"
	fi
fi

