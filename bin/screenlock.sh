#!/bin/bash

#colors
black=282828
red=cc241d
green=98971a
yellow=d79921
blue=458588
purple=b16286
aqua=689d6a
grey=a89984
lblack=928374
lred=fb4934
lgreen=b8bb26
lyellow=fabd2f
lblue=83a598
lpurple=d3869b
laqua=8ec07c
lgrey=ebdbb2
orange=d65d04
lorange=fe8019

op=77

if [[ -f $HOME/.noss ]]; then
	exit 0
fi

revert() {
	xset dpms 300 300 300
}

trap revert HUP INT TERM

xset +dpms dpms 5 5 5

i3lock --nofork \
	--keylayout 2 \
	--pass-media-keys \
	--blur 6 \
	--veriftext="..." \
	--radius 128 \
	--ring-width 8 \
	--indicator --clock \
	--timesize=30 \
	--datesize=16 \
	--verifsize=16 \
	--layoutsize=16 \
	--timestr="%H:%M:%S" \
	--datestr="%d-%m-%Y" \
	--linecolor=${black}22 \
	--datecolor=${lorange}ff --timecolor=${lorange}ff --layoutcolor=${lorange}ff \
	--separatorcolor=${lyellow}ff --keyhlcolor=${lorange}${op} --bshlcolor=${lred}${op} \
	--ringcolor=${orange}${op} --insidecolor=${black}${op} \
	--verifcolor=${grey}ff \
	--ringvercolor=${grey}${op} --insidevercolor=${black}${op} \
	--wrongcolor=${lred}ff \
	--ringwrongcolor=${lred}${op} --insidewrongcolor=${red}${op}

revert

