#!/usr/bin/env python3

import os
import sys
import yaml
import requests

r = requests.get('http://192.168.0.1/keys.yml')
if r.status_code != requests.codes.ok:
    print('error: %n\n' % (r.status_code))
    sys.exit(1)

args = sys.argv[1:]

keys = yaml.safe_load(r.text)

d = os.path.expanduser('~/.ssh')
if not os.path.exists(d):
    os.mkdir(d, 0o700)

s = ''
for k in keys:
    if len(args) > 0:
        found = False
        for a in args:
            if a in k['tags']:
                found = True
                break
        if not found:
            continue

    s += '%s %s\n' % (k['key'], k['name'])

if s != '':
    print(s)
    keys_file = os.path.join(d, 'authorized_keys')
    if os.path.isfile(keys_file):
        os.rename(keys_file, os.path.join(d, 'authorized_keys.old'))
    with open(keys_file, 'w') as f:
        f.write(s)

os.chmod(os.path.join(d, 'authorized_keys'), 0o600)
