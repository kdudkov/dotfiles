setlocal foldmethod=syntax
setlocal foldlevel=2
setlocal foldnestmax=4
setlocal tabstop=4
setlocal softtabstop=4
setlocal shiftwidth=4
setlocal textwidth=150
setlocal expandtab
setlocal smarttab
setlocal nosmartindent

nnoremap <F9> :w<cr>:exec '!python3' shellescape(@%, 1)<cr>
nnoremap <leader>r :w<cr>:exec '!python3' shellescape(@%, 1)<cr>
nnoremap <leader>R :w<cr>:exec '!env/bin/python' shellescape(@%, 1)<cr>
