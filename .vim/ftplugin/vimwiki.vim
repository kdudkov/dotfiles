:UltiSnipsAddFiletypes vimwiki.markdown

imap <silent> [[ [[<esc><Plug>ZettelSearchMap
nmap T <Plug>ZettelYankNameMap
xmap z <Plug>ZettelNewSelectedMap
nmap gZ <Plug>ZettelReplaceFileWithLink
nmap <leader>s :ZettelOpen<cr>
