set nocompatible
filetype off
set rtp+=~/.fzf

if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim 
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin()

Plug 'morhetz/gruvbox'
Plug 'arcticicestudio/nord-vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

Plug 'SirVer/ultisnips'
Plug 'jceb/vim-orgmode'
Plug 'tpope/vim-speeddating'
Plug 'mattn/calendar-vim'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-sensible'
Plug 'airblade/vim-gitgutter'
Plug 'maralla/completor.vim'
Plug 'fatih/vim-go', { 'tag': '*', 'do': ':GoUpdateBinaries' }
" On-demand loading
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'lervag/vimtex'
Plug 'AndrewRadev/splitjoin.vim'
Plug 'vimwiki/vimwiki'
Plug 'michal-h21/vim-zettel'
Plug 'conornewton/vim-pandoc-markdown-preview'
call plug#end()

filetype plugin indent on
filetype on

" ############ colors
set background=dark
let base16colorspace=256
set t_Co=256
set termguicolors
"colorscheme gruvbox
colorscheme nord

" Use the OS clipboard by default (on versions compiled with `+clipboard`)
set clipboard=unnamed
" Enhance command-line completion
set wildmenu
" Allow backspace in insert mode
set backspace=indent,eol,start
" Add the g flag to search/replace by default
set gdefault
" Change mapleader
let mapleader="\\"
"let mapleader="\<space>"
" Don’t add empty newlines at the end of files
set binary
set noeol
" Centralize backups, swapfiles and undo history
"set backupdir=~/.vim/backup
"set directory=~/.vim/swap
if exists("&undodir")
	set undodir=~/.vim/undo
endif

" Don’t create backups when editing files in certain directories
set backupskip=/tmp/*,/private/tmp/*

" General settings
set number
set encoding=utf8 nobomb
set ttyfast
set showmatch
set clipboard=unnamed
set exrc
set secure
set hidden
set tabstop=4
set shiftwidth=4
set smarttab
"set expandtab
set autoindent
set modeline
set modelines=4
syntax on

" Show “invisible” characters
set lcs=tab:▸\ ,trail:·,eol:¬,nbsp:_
set list
" Highlight searches
set hlsearch
" Ignore case of searches
set ignorecase
" Highlight dynamically as pattern is typed
set incsearch
" Always show status line
set laststatus=2
" Enable mouse in all modes
set mouse=a
" Disable error bells
set noerrorbells
" Don’t reset cursor to start of line when moving around.
set nostartofline
" Show the cursor position
set ruler
" Don’t show the intro message when starting Vim
set shortmess=atI
" Show the current mode
set showmode
" Show the filename in the window titlebar
set title
" Show the (partial) command as it’s being typed
set showcmd
" Start scrolling three lines before the horizontal window border
set scrolloff=3
set splitbelow splitright

set langmap=ФИСВУАПРШОЛДЬТЩЗЙКЫЕГМЦЧНЯЖ;ABCDEFGHIJKLMNOPQRSTUVWXYZ:,фисвуапршолдьтщзйкыегмцчня;abcdefghijklmnopqrstuvwxyz,ё\\\
"set keymap=russian-jcukenmac
set iminsert=0

" Strip trailing whitespace (,ss)
function! StripWhitespace()
	let save_cursor = getpos('.')
	let old_query = getreg('/')
	:%s/\s\+$//e
	call setpos('.', save_cursor)
	call setreg('/', old_query)
endfunction
noremap <leader>ss :call StripWhitespace()<CR>
" Save a file as root
noremap <leader>W :w !sudo tee % > /dev/null<CR>

if has("gui_macvim")
	set guifont=Iosevka\ Term\ Medium:h14
else
	set guifont=Iosevka\ Term\ Medium\ 12
endif

" Airline
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#show_buffers = 1
let g:airline#extensions#tabline#show_tabs = 1
let g:Powerline_symbols='unicode'
"let g:airline#extensions#xkblayout#enabled = 0

" tex
let g:latex_view_general_viewer = 'zathura'
let g:vimtex_view_method = 'zathura'
let g:tex_flavor = 'latex'
let g:vimtex_quickfix_open_on_warning = 0
let g:vimtex_quickfix_mode = 2
let g:vimtex_compiler_method = 'latexmk'
"set conceallevel=1
"let g:tex_conceal='abdmg'

" markdown
let g:md_pdf_viewer="zathura"

" go
let g:org_agenda_files=['~/org/index.org']
let g:go_fmt_command = 'goimports'
let g:go_def_mode='gopls'
let g:go_info_mode='gopls'

" snipmate
" let g:snipMate.scope_aliases['markdown'] = 'markdown,vimwiki'
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"

" wimwiki
let g:vimwiki_global_ext = 0
let g:vimwiki_markdown_link_ext = 1
let g:vimwiki_table_mappings = 0
let g:vimwiki_root = $HOME . '/Yandex.Disk/vimwiki/'
let g:vimwiki_list = [{'path': $HOME . '/Yandex.Disk/vimwiki/', 'syntax': 'markdown', 'ext': '.md'}]
let g:zettel_format = "%y%m%d-%H%M-%title"

" Spelling
set spelllang=ru,en_us
nnoremap <F3> :setlocal spell!<cr>
inoremap <C-l> <c-g>u<Esc>[s1z=`]a<c-g>u
hi clear SpellBad
hi SpellBad cterm=underline ctermfg=red
hi SpellBad gui=undercurl

" Relative Numbering
nnoremap <F4> :set relativenumber!<CR>

nnoremap  ]b :bp<CR>
nnoremap  [b :bn<CR>
" delete buffer
nnoremap <leader>x :w<cr>:bd<CR>
" fold
nnoremap <space> za
vnoremap <space> zf

" smart move
nnoremap j gj
nnoremap k gk
vnoremap j gj
vnoremap k gk

nnoremap <silent> <F12> :w<cr>:bn<cr>
nnoremap <silent> <S-F12> :w<cr>:bp<cr>

nmap <leader>f :GFiles<cr>
nmap <leader>F :Files<cr>
nmap <leader>b :Buffers<cr>
nmap <leader>h :History<cr>
nmap <leader>l :BLines<cr>
nmap <leader>L :Lines<cr>
nmap <leader>' :Marks<cr>
nmap <leader>a :Ag<Space>

"inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
"inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
"inoremap <expr> <cr> pumvisible() ? "\<C-y>\<cr>" : "\<cr>"

nnoremap <leader><tab> gg=G''zz :call StripWhitespace()<CR>

" Nerdtree
autocmd StdinReadPre * let s:std_in=1
nmap <F5> :NERDTreeToggle<CR>

vnoremap <silent><Leader>y "yy :call system('xclip -selection clipboard', @y)<CR>

