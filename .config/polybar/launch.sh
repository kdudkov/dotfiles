#!/bin/bash

cd $(dirname ${BASH_SOURCE[0]})

bar="desktop"
[[ -f ~/.config/bar ]] && bar=$(cat ~/.config/bar)

wired=""
wireless=""

for i in $(ip route | awk '/default/ { print $5 }'); do
	if [[ "$i" == wl* ]]; then
		echo "found wireless card $i"
		export wireless="$i"
	else
		echo "found wired card $i"
		export wired="$i"
	fi
done

cat config.tpl | envsubst > config

mons=($(bspc query --names -M))

echo "Using $bar"

killall -q polybar
while pgrep -x polybar >/dev/null; do sleep 1; done

for m in ${mons[*]}; do
	MONITOR=$m polybar ${bar} &
done;