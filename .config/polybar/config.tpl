[colors]
nord0 = "#2E3440"
nord1 = "#3B4252"
nord2 = "#434C5E"
nord3 = "#4C566A"
nord4 = "#D8DEE9"
nord5 = "#E5E9F0"
nord6 = "#ECEFF4"
nord7 = "#8FBCBB"
nord8 = "#88C0D0"
nord9 = "#81A1C1"
nord10= "#5E81AC"
nord11= "#BF616A"
nord12= "#D08770"
nord13= "#EBCB8B"
nord14= "#A3BE8C"
nord15= "#B48EAD"

black = ${colors.nord1}
red = ${colors.nord11}
green = ${colors.nord14}
yellow = ${colors.nord13}
blue = ${colors.nord9}
purple = ${colors.nord15}
aqua = ${colors.nord8}
grey = ${colors.nord5}
lblack = ${colors.nord3}
lred = ${colors.nord11}
lgreen = ${colors.nord14}
lyellow = ${colors.nord13}
lblue = ${colors.nord9}
lpurple = ${colors.nord15}
laqua = ${colors.nord7}
lgrey = ${colors.nord6}


orange = ${colors.nord12}
lorange = ${colors.nord12}

background = ${colors.nord1}
background-alt = ${colors.nord2}
foreground = ${colors.nord9}
foreground-icon = ${colors.nord4}
alert = ${colors.nord11}

[bar/common]
width = 100%
height = 28
;radius = 6.0
fixed-center = true
background = ${colors.background}
foreground = ${colors.foreground}
line-size = 3
line-color = #f00
border-size = 1
border-color = #00000000
padding-left = 0
padding-right = 2
module-margin-left = 1
module-margin-right = 2
tray-position = right
tray-padding = 2
tray-background = ${colors.nord2}

wm-restack = bspwm
;wm-restack = i3

;override-redirect = true

scroll-up = bspwm-desknext
scroll-down = bspwm-deskprev

;scroll-up = i3wm-wsnext
;scroll-down = i3wm-wsprev

font-0 = Noto Sans:pixelsize=13;2
font-1 = Material Design Icons Desktop:pixelsize=18;4
font-2 = Material Design Icons Desktop:pixelsize=14;-2
font-3 = Iosevka Medium:pixelsize=13;0

cursor-click = pointer
cursor-scroll = ns-resize

locale = ru_RU.UTF-8

[bar/desktop]
inherit = bar/common
monitor = ${env:MONITOR:DP-1}
modules-left = bspwm xwindow
modules-center = date
modules-right = pulseaudio eth memory cpu temperature updates xkeyboard

[bar/note]
inherit = bar/common
monitor = ${env:MONITOR:DP-1}
modules-left = bspwm xwindow
modules-center = date
modules-right = pulseaudio eth wlan cpu temperature battery updates xkeyboard

[bar/work]
inherit = bar/common
monitor = ${env:MONITOR:DP-1}
modules-left = bspwm xwindow
modules-center = date calendar
modules-right = pulseaudio eth wlan cpu temperature battery updates xkeyboard

[module/xwindow]
type = internal/xwindow
label = %title:0:50:...%

[module/xkeyboard]
type = internal/xkeyboard
format = <label-layout> <label-indicator>
format-prefix = 󰥻
format-prefix-foreground = ${colors.foreground-icon}

label-layout = %icon%
layout-icon-0 = us;US
layout-icon-1 = ru;%{F#bf616a}RU%{F-}

blacklist-0 = num lock
label-indicator-on = %name%
label-indicator-on-padding = 2
label-indicator-on-foreground = ${colors.background}
label-indicator-on-background = ${colors.foreground}

[module/filesystem]
type = internal/fs
interval = 25

mount-0 = /

label-mounted = %{F#0a81f5}%mountpoint%%{F-}: %percentage_used%%
label-unmounted = %mountpoint% not mounted
label-unmounted-foreground = ${colors.foreground-icon}

[module/bspwm]
type = internal/bspwm
ws-icon-0 = 1;󰎦
ws-icon-1 = 2;󰎩
ws-icon-2 = 3;󰎬
ws-icon-3 = 4;󰎮
ws-icon-4 = 5;󰎰
ws-icon-5 = 6;󰎵
ws-icon-6 = 7;󰎸
ws-icon-7 = 8;󰎻
ws-icon-8 = 9;󰎾
ws-icon-9 = 10;󰽾

ws-icon-default = •

label-focused = %icon%
label-focused-foreground = ${colors.nord0}
label-focused-background = ${colors.nord14}
;label-focused-underline= ${colors.orange}
label-focused-padding = 1

label-occupied = %icon%
label-occupied-foreground = ${colors.nord6}
label-occupied-padding = 1

label-urgent ="%icon%%{F#bf616a}%{T3}%{O-12}󰀨 %{T-}%{F-}"
label-urgent-foreground = ${colors.nord6}

label-empty = %icon%
label-empty-foreground = ${colors.nord3}
label-empty-padding = 1

; Separator in between workspaces
; label-separator = |

[module/redshift]
type = custom/script
interval = 15
exec = ~/.config/polybar/modules/redshift.sh
format-prefix = " "
format-prefix-foreground = ${colors.foreground-icon}
label = %output%
click-left = ~/.config/polybar/modules/redshift.sh toggle

[module/yndx]
type = custom/script
interval = 300
exec = ~/.config/polybar/modules/yndx.sh
format-prefix = "󰄪 "
format-prefix-foreground = ${colors.foreground-icon}
label = %output%

[module/calendar]
type = custom/script
interval = 60
exec = ~/.config/polybar/modules/cal
label = %output%

[module/mpd]
type = internal/mpd
format-online = <label-song>  <icon-prev> <icon-stop> <toggle> <icon-next>

icon-prev = 
icon-stop = 
icon-play = 
icon-pause = 
icon-next = 

label-song-maxlen = 25
label-song-ellipsis = true

[module/xbacklight]
type = internal/xbacklight

format-prefix = " "
format-prefix-foreground = ${colors.foreground-icon}
format = <label>
label = %percentage%%

bar-width = 10
bar-indicator = |
bar-indicator-foreground = #fff
bar-indicator-font = 2
bar-fill = ─
bar-fill-font = 2
bar-fill-foreground = #9f78e1
bar-empty = ─
bar-empty-font = 2
bar-empty-foreground = ${colors.foreground}

[module/backlight-acpi]
inherit = module/xbacklight
type = internal/backlight
card = intel_backlight

[module/cpu]
type = internal/cpu
interval = 2
format-prefix = 󰻠
format-prefix-foreground = ${colors.foreground-icon}
label = %percentage:2%%

[module/memory]
type = internal/memory
interval = 2
format-prefix = 󰍛
format-prefix-foreground = ${colors.foreground-icon}
label = %percentage_used%%

[module/wlan]
type = internal/network
interface = ${wireless}
interval = 3.0

format-connected-prefix = 󰖩
format-connected-prefix-foreground = ${colors.foreground-icon}
format-connected = <ramp-signal> <label-connected>
label-connected = [%essid%] %local_ip%

format-disconnected-prefix = 󰤯
format-disconnected-prefix-foreground = ${colors.foreground-icon}
format-disconnected = <label-disconnected>
label-disconnected = %ifname%
label-disconnected-foreground = ${colors.alert}

ramp-signal-0 = 󰲟
ramp-signal-1 = 󰲡
ramp-signal-2 = 󰲣
ramp-signal-3 = 󰲥
ramp-signal-4 = 󰲧
ramp-signal-5 = 󰲩
ramp-signal-6 = 󰲫
ramp-signal-7 = 󰲭
ramp-signal-8 = 󰲯
ramp-signal-9 = 󰲱
ramp-signal-10 = 󰿭
ramp-signal-0-foreground = ${colors.alert}
ramp-signal-1-foreground = ${colors.alert}
ramp-signal-2-foreground = ${colors.alert}
ramp-signal-3-foreground = ${colors.yellow}
ramp-signal-4-foreground = ${colors.yellow}
ramp-signal-5-foreground = ${colors.yellow}
ramp-signal-6-foreground = ${colors.yellow}
ramp-signal-7-foreground = ${colors.green}
ramp-signal-8-foreground = ${colors.green}
ramp-signal-9-foreground = ${colors.green}
ramp-signal-10-foreground = ${colors.green}

ramp-signal-foreground = ${colors.foreground}

[module/eth]
type = internal/network
interface = ${wired}
interval = 3.0

format-connected-prefix = 󰛳 
format-connected-prefix-foreground = ${colors.foreground-icon}
label-connected = %local_ip%

format-disconnected-prefix = 󰲛
format-disconnected-prefix-foreground = ${colors.foreground-icon}
format-disconnected = <label-disconnected>
label-disconnected = %ifname%
label-disconnected-foreground = ${colors.alert}

[module/date]
type = internal/date
interval = 5

date =
date-alt = " %d.%m.%Y"
time = %H:%M
time-alt = %H:%M

format-prefix = 󰃰
format-prefix-foreground = ${colors.foreground-icon}

label = %date% %time%

[module/pulseaudio]
type = internal/pulseaudio

format-volume = <ramp-volume> <label-volume>
label-volume = %percentage%%
label-volume-foreground = ${root.foreground}

format-muted = <label-muted>
label-muted = 󰝟
label-muted-foreground = #666

ramp-volume-0 = 󰕿
ramp-volume-1 = 󰖀
ramp-volume-2 = 󰕾
ramp-volume-foreground = ${colors.foreground-icon}

click-right = pavucontrol &

[module/battery]
type = internal/battery
battery = BAT0
adapter = AC
full-at = 98
time-format = %H:%M

format-charging-prefix = 󰢝
format-charging-prefix-foreground = ${colors.green}
format-charging = <label-charging>
label-charging = "%percentage%%"

format-discharging = <ramp-capacity> <label-discharging>
label-discharging = "%percentage%% %time%"

format-full-prefix = 󰂄
format-full-prefix-foreground = ${colors.foreground-icon}
format-full = <label-full>
label-full = full

ramp-capacity-0 = 󰂎
ramp-capacity-1 = 󰁻
ramp-capacity-2 = 󰁼
ramp-capacity-3 = 󰁽
ramp-capacity-4 = 󰁾
ramp-capacity-5 = 󰁿
ramp-capacity-6 = 󰂀
ramp-capacity-7 = 󰂁
ramp-capacity-8 = 󰂂
ramp-capacity-9 = 󰁹
ramp-capacity-foreground = ${colors.foreground-icon}

[module/temperature]
type = internal/temperature
thermal-zone = 0
warn-temperature = 60

format-prefix = 󰈸
format-prefix-foreground = ${colors.foreground-icon}
format = <label>
format-warn = <label-warn>
label = %temperature-c%

format-warn-prefix = 󰈸
format-warn-prefix-foreground = ${colors.orange}
label-warn = %temperature-c%
label-warn-foreground = ${colors.orange}

ramp-0 = 
ramp-1 = 
ramp-2 = 
ramp-foreground = ${colors.foreground-icon}

[module/updates]
type = custom/script
interval = 600
format-prefix = 󰭽
format-prefix-foreground = ${colors.foreground-icon}
exec = ~/.config/polybar/modules/updates.sh
label = %output%

[module/powermenu]
type = custom/menu

expand-right = true

format-spacing = 1

label-open = 
label-open-foreground = ${colors.orange}
label-close =  cancel
label-close-foreground = ${colors.foreground-icon}
label-separator = |
label-separator-foreground = ${colors.foreground-icon}

menu-0-0 = reboot
menu-0-0-exec = menu-open-1
menu-0-1 = power off
menu-0-1-exec = menu-open-2

menu-1-0 = cancel
menu-1-0-exec = menu-open-0
menu-1-1 = reboot
menu-1-1-exec = sudo reboot

menu-2-0 = power off
menu-2-0-exec = sudo poweroff
menu-2-1 = cancel
menu-2-1-exec = menu-open-0

[settings]
screenchange-reload = true
;compositing-background = xor
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over
;pseudo-transparency = false

[global/wm]
margin-top = 5
margin-bottom = 5

; vim:ft=dosini
