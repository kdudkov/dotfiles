#!/bin/bash

plfonts="no"

os=$(uname -s)

echo "We are on $os"
echo

for f in $(cat files_to_save); do
	cp -u -v $f $HOME/$f
done;

for k in .config bin .Xresources.d; do
	echo "Copy -r $k"
	cp -u -v -r $k $HOME/
done

if [[ $os == "Darwin" ]]; then
	echo "copy .profile (for mac only)"
	cp -u -v .profile $HOME
fi

echo "Copy .config"
cp -r .config $HOME/

echo "Updating .bashrc"
for e in .bash_ext .bash_local; do 
	str="[ -f ~/$e ] && source ~/$e"
	if grep -q "$str" ~/.bashrc; then
		echo -n
	else
		echo "Adding $e..."
		echo $str >> ~/.bashrc
	fi
done

source vim-configure.sh

fontdir=""

if [[ $os == "Darwin" ]]; then
	fontdir=$HOME/Library/Fonts
fi

if [[ $os == "Linux" ]]; then
	fontdir=$HOME/.fonts
fi

if [[  -n $fontdir && -d $fontdir ]]; then
	echo "fonts dir: $fontdir"
	cp fonts/*.ttf $fontdir

	for a in Bold Light Medium Regular Retina; do
		echo "getting FiraCode-$a.ttf..."
		curl -s "https://github.com/tonsky/FiraCode/blob/master/distr/ttf/FiraCode-$a.ttf?raw=true" -o $fontdir/FiraCode-$a.ttf
	done

	for a in AnkaCoder AnkaCoderNarrow AnkaCoderCondensed; do
		echo "getting $a..."
		curl -s "https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/anka-coder-fonts/$a.zip" -o font.zip
		unzip -o font.zip -d $fontdir -x *.txt
		rm font.zip
	done

	echo "Getting Jetbrains Mono"
	curl -s "https://download-cf.jetbrains.com/fonts/JetBrainsMono-1.0.0.zip" -o font.zip
	unzip -o font.zip -d $fontdir
	rm font.zip

	if [[ $plfonts == "yes" ]]; then
		pushd $HOME

		git clone https://github.com/powerline/fonts.git
		find $HOME/fonts -name *.ttf -exec cp {} $fontdir \;
		rm -rf fonts

		popd
	fi
fi
